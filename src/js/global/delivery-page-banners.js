/* update text on Delivery options page - added by sush */
if (thePageId === "delivery"){
  var node = document.createElement("P");
  var textnode = document.createTextNode("Select our fixed 1 hour slots for guaranteed delivery within the exact 60 minutes of your choice. For a cheaper, greener option, you can pick our Flexi saver 4 hour window right now. We'll confirm your 1 hour time slot by text on the day of delivery, so you won't have to wait in any longer than you need to. We can reserve your chosen slot for 2 hours. This should give you plenty of time to place your order.");
  node.appendChild(textnode);
  node.setAttribute('class', 'reservationComment');
  node.style.width = "100%";
  var testElements = document.getElementById('reserveInfo').firstChild;
  testElements.appendChild(node);


  /* NON DS Banner Targeting */
  // var DSbanner = document.querySelectorAll(".haveSubs")[0];if (!DSbanner){var deliverySlots = document.getElementById("deliverySlots"),bannerLink = document.createElement("a"),bannerImg = document.createElement("img"); if (deliverySlots){bannerLink.href = "http://www.tesco.com/deliverysaver/?icid=Delivery_Options_home_delivery";bannerLink.style.display = "block";bannerLink.style.margin = "15px 0";bannerLink.setAttribute("target", "_blank");bannerImg.style.width = "100%";bannerImg.style.display = "block";bannerImg.src = '/groceries/MarketingContent/Sites/Retail/superstore/mercury/P/i/DeliveryOption/deliverysaver-banner-2.jpg';bannerLink.appendChild(bannerImg);deliverySlots.insertBefore(bannerLink, deliverySlots.firstChild);}}
  /* Home Del Banner Targeting */
  var DSbanner01 = document.querySelectorAll(".postCode")[0]; if(DSbanner01) { DSbanner01 = DSbanner01.innerHTML; if(DSbanner01 == "B15 2QW" || DSbanner01 == "PR1 7BG" ){ var deliverySlots01 = document.getElementById("home__del__option"),bannerLink01 = document.createElement("a"),bannerImg01 = document.createElement("img"); if (deliverySlots01){bannerLink01.href = "http://www.tesco.com/deliverysaver?icid=BookSlot_NonDS_banner"; bannerLink01.style.display = "block"; bannerLink01.style.margin = "15px 0"; bannerLink01.setAttribute("target", "_blank"); bannerImg01.style.width = "100%"; bannerImg01.style.display = "block"; bannerImg01.src = 'http://preview.tesco.com/groceries/MarketingContent/Sites/Retail/superstore/mercury/p/i/DeliveryOption/CnC-Book-a-slot-2.jpg'; bannerLink01.appendChild(bannerImg01);deliverySlots01.insertBefore(bannerLink01, deliverySlots01.firstChild);}}}

}
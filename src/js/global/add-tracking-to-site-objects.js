/* Add tracking to Carousel Elements */
if(thePageId === "department"){
  var carouselSlides = document.getElementById("carouselSlides");
  if(carouselSlides){
    var department = document.getElementById("intro") || document.getElementsByTagName("H1")[0]; 
    if(department){
      department = department.innerHTML.toLowerCase().replace(/ /,"_").replace(/&/,"_").replace(/<span>|<\/span>/gmi,""); 
      var i, j, aHref1, aHref2, aHref3, carouselSlides = carouselSlides.getElementsByTagName("li"), prodId;
      
      for (i = 0; i < carouselSlides.length; i++){
        /* Get Prod Id */
        prodId = carouselSlides[i].getAttribute('data-product-id');
        aHref = carouselSlides[i].getElementsByTagName("a");
        
        for (j = 0; j < aHref.length; j++){
          /* Set new href with tagging */
          aHref[j].href = aHref[j].href + "&icid=CARO_" + department + "_" + aHref[j].href.substring(77) + "_" + prodId;
        }
      }
    }
  }
}

/* Replace Header Links */
if(document.getElementById("primaryNav")){
  var thirdMainNav = document.getElementById("primaryNav").getElementsByTagName('a')[2];
  if(thirdMainNav){thirdMainNav.setAttribute("target", "_blank");}
  
  var sixthMainNav = document.getElementById("primaryNav").getElementsByTagName('a')[5];
  if(sixthMainNav){sixthMainNav.setAttribute("href", "http://www.tesco.com/deliverysaver/?icid=TopTab_delivery_Saver");}
}


/* Adding Tagging to Header */
function addClickTrackNav(preface){return function(){s.tl(this,'o',preface + this.theInnerText);};}
function trackNavLinks(targetId, preface){if (thePageId !== "delivery"){var theNav = document.getElementById(targetId), i;if(theNav){var theLinks = theNav.getElementsByTagName("a");for (i = 0; i < theLinks.length; i++) {theLinks[i].theInnerText = theLinks[i].textContent.replace(/ /g, '_') || theLinks[i].innerText.replace(/ /g, '_') || ""; if(theLinks[i].parentNode.className == " seasonal"){theLinks[i].theInnerText = "Seasonal";}addEvent(theLinks[i],'click',addClickTrackNav(preface));}}} else {return false;}}
trackNavLinks("userNav","TextNav_");/*trackNavLinks("primaryNav","TopNav_");trackNavLinks("secondaryNav","SecondaryNav_");*/

/* Add tracking to Grid-List View */
function addClickTrackView(type){return function(){s.tl(this,'o',"View_"+type.replace('mode', '').replace('Line', 'List'));};}
(function(){if (thePageId == "product_browse"){var gridListContainer = document.querySelectorAll("div.productListMode"),resultArray = [],miniAResult,i;if(gridListContainer){for (i = 0; i < gridListContainer.length; i++) {miniAResult = gridListContainer[i].getElementsByTagName("a");for (var j=0; j<miniAResult.length; j++) {resultArray.push(miniAResult[j]);}}for (i = 0; i < resultArray.length; i++) {var type = resultArray[i].className;addEvent(resultArray[i],'click',addClickTrackView(type));}}}})();

/* Add tracking to PDP Healthy - Alternatives */
function addClickTrackPanel(theHref){return function(){s.tl(this,'o',theHref);};}
(function(){if (thePageId == "product_details"){
var panelLinksCont = document.querySelectorAll("ul.panelLinks")[0], theHref, panelLinks;if(panelLinksCont){panelLinks = panelLinksCont.getElementsByTagName("a");if(panelLinks){for (i = 0; i < panelLinks.length; i++) {theHref = panelLinks[i].getAttribute("href").replace('#', '');addEvent(panelLinks[i],'click',addClickTrackPanel(theHref));}}}}})();

/* Add tracking to breadcrumbs */
function addClickTrackBreadcrumbNav(){return function(){
  s.linkTrackVars="events,eVar45";
  s.linkTrackEvents = "event45";
  s.events += ",event45";
  s.eVar45="breadcrumb:" + this.theInnerText;
  s.tl(this,'o',"breadcrumb:" + this.theInnerText);
};}
(function(){
  var breadcrumbNav = document.getElementById("breadcrumbNav");
  if(breadcrumbNav){
    var breadcrumbNavA = breadcrumbNav.getElementsByTagName("a");
    for (i = 0; i < breadcrumbNavA.length; i++) {
      breadcrumbNavA[i].theInnerText = breadcrumbNavA[i].textContent.toLowerCase() || breadcrumbNavA[i].innerText.toLowerCase() || ""; 
      addEvent(breadcrumbNavA[i],'click',addClickTrackBreadcrumbNav());
    }
    }
})();

/* Add tracking to Secondary Nav - level 3 */
function addClickTracksecondaryNav(){return function(){
  if(this.alreadyClicked == true) {
    return false;
  } else {
    this.alreadyClicked = true;
    s.linkTrackVars="events,eVar45";
    s.linkTrackEvents = "event45";
    s.events += ",event45";
    s.eVar45="navigation:" + this.theInnerText;
    s.tl(this,'o',"navigation:" + this.theInnerText);
  }
};}
(function(){
  var secondaryNav = document.getElementById("secondaryNav");
  if(secondaryNav){
      addEvent(secondaryNav,'DOMSubtreeModified',function () {

        var level_four_a = secondaryNav.getElementsByClassName("level4 departmentLink");
        for (i = 0; i < level_four_a.length; i++) {
          level_four_a[i].theInnerText = level_four_a[i].textContent.toLowerCase() || level_four_a[i].innerText.toLowerCase() || "";
          level_four_a[i].alreadyClicked = false;
          addEvent(level_four_a[i],'click',addClickTracksecondaryNav());
        } 
    });
  }
})();

/* Add tracking to BookASlot - initiated on-load */
function addClickTrackBookSlot() {return function(){s.tl(this,'o','Basket_bookSlot');};}
function addClickTrackEvar() {var appendLetters, theEvar;if (thePageId === "delivery") {appendLetters = "-hd"; } else {appendLetters = "-cc"; }setTimeout(function () {theEvar = s.eVar6;if ((typeof theEvar !== "undefined") && (theEvar !== null) && (theEvar.length > 0)){s.eVar6="";s.tl(this,'o',theEvar + appendLetters);addTrackBookSlot();}}, 3000);}
function addTrackBookSlot() {'use strict';if (thePageId === "delivery" || thePageId === "collectiondelivery") {
var deliveryRangeGrid = document.getElementById("deliveryRangeGrid"),i; if (deliveryRangeGrid){deliveryRangeGrid = deliveryRangeGrid.querySelectorAll(".slotDescription");for (i = 0; i < deliveryRangeGrid.length; i++) {addEvent(deliveryRangeGrid[i], 'click', addClickTrackEvar);}}}}
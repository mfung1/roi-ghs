/* 
  Global Marketing File. Added across all pages except chckout.
  Global marketing add event , or use YUI().use('event', function (Y) { Y.on('domready', function(){   runScriptHere   }); }); for on page load 
*/

// Add Event Function
function addEvent(el, evt, func) {
  if (el.addEventListener){
    return el.addEventListener(evt, func, false);
  } else if(el.attachEvent){
    return el.attachEvent('on' + evt, func);
  }
}


/* Global Variables (Page locator) */
var thePageId = document.getElementsByTagName('body')[0].id;
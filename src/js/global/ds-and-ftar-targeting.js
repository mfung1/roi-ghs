
var isTrialPlan = JSON.parse(document.getElementById('configData').innerHTML).deliverySaver.IsTrialPlan;
var subscription_Status = JSON.parse(document.getElementById('configData').innerHTML).deliverySaver.subcriptionStatus;
var isFirstTimeShopper = JSON.parse(document.getElementById('configData').innerHTML).isFirstTimeShopper;


// Find the dif in days
function diffDays(endDate){
  var p = JSON.stringify(new Date());	//Save today's date in p Javascript format
	var pt = p.toString().substring(1,11); //Save today's date in string format YYYY-MM-DD
	var e = endDate; //Fetch subscription end date from configData
	var et = e.toString().substring(0,10); //Save subscription end date in string format YYYY-MM-DD
	var days = Math.abs((new Date(et)) - (new Date(pt))); //difference between subscription end date and current date. In milliseconds
	var day = ((days / (1000*60*60*24)) -1 ); //Convert milliseconds to days. Gives faction value
	return day.toString().substring(0,2); //Rounding off fraction value to perfect number
}


// Function to replace TEXT
function textChangeDS(arrayList, attribute){
  var i,altText;

  // For each element in array, replace text
  for (i = 0; i < arrayList.length; i++) {

    altText = arrayList[i].getAttribute(attribute);
    arrayList[i].innerHTML = altText;

    // If there is XX in the text, replace with countdown
    if (altText && altText.indexOf("XX") > -1) {
      var countDownDays = diffDays(JSON.parse(document.getElementById('configData').innerHTML).deliverySaver.subcriptionEndDate),
        updatedDate = altText.replace("XX", countDownDays);
      arrayList[i].innerHTML = updatedDate;
    }

  }

}


// Function to replace URLs within targeting
// Acceptes three args, the array, the href data attribute, the ICID data attribute
function urlChangeDS(arrayList, hrefAttribute, icidAttibute){

  var i;

  for (i = 0; i < arrayList.length; i++) {

    //Create a temp URL
    var tempUrlArray = arrayList[i].href.split("icid="),
      ftsString = isFirstTimeShopper == "True" ? "FTS_" : "";

     
     // If hidden, show 
    if (arrayList[i].style.display == "none") {
      arrayList[i].style.display = "block";
    }
    arrayList[i].setAttribute('target', '_blank');

    // If there is both a replacement ICID or replacement HREF -- update HREF:
    if(arrayList[i].hasAttribute(icidAttibute) && arrayList[i].hasAttribute(hrefAttribute)) {
      arrayList[i].href = arrayList[i].getAttribute(hrefAttribute) + "icid=" + ftsString + arrayList[i].getAttribute(icidAttibute);
    
    // If there is just a replacement ICID -- update HREF:
    } else if (arrayList[i].hasAttribute(icidAttibute)) {
      arrayList[i].href = tempUrlArray[0] + "icid=" + ftsString + arrayList[i].getAttribute(icidAttibute);

    // If there is just a replacement HREF -- update HREF:
    } else if (arrayList[i].hasAttribute(hrefAttribute)){
      arrayList[i].href = arrayList[i].getAttribute(hrefAttribute) + "icid=" + ftsString + tempUrlArray[1];
    }

    if (arrayList[i].getAttribute(hrefAttribute) == "#") {
      arrayList[i].removeAttribute("href");
    }

  }

}


// DS AND FTAR Targeting

// Is a General FTAR customer
if ((isTrialPlan == "true") && (subscription_Status == "PendingStop")) {
  
  var hasDataAtt = document.querySelectorAll('[data-FTAR-text]'), // For each data attribute element 
      hasAlthref = document.querySelectorAll('.has-alt-ftar-href'); // For each alt HREF classname (may include ether href or icid attribute )
  
  // Change any text elements 
  if(hasDataAtt) {
    textChangeDS(hasDataAtt, "data-FTAR-text");
  }

  if(hasAlthref){
    urlChangeDS(hasAlthref, "data-alt-ftar-href", "data-alt-ftar-icid");
  }

  // is a FTAR FTS customer  
  if (isFirstTimeShopper == "True") {
    // Code
  }

  // THIS IS A PAID DS CUSTOMER
} else if ((isTrialPlan == "false") && (subscription_Status == "Active")) {
  

  var paidDSAltTextElements = document.querySelectorAll('[data-paid_ds-text]'), // For each text attribute 
    paidDSAltHREFElements = document.querySelectorAll('.has-alt-paid_ds-href'); // For each classname

  // Change any text elements
  if(paidDSAltTextElements) {
    textChangeDS(paidDSAltTextElements, "data-paid_ds-text");
  }

  // Change URL elements
  if(paidDSAltHREFElements){
    urlChangeDS(paidDSAltHREFElements, "data-alt-paid_ds-href", "data-alt-paid_ds-icid");
  }

}

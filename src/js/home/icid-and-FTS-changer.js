
// Allows the same content to be across the different homepages

function hasClass(el, className){
if (el.classList)
  return el.classList.contains(className);
else
  return new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
}

var d_Atag = document.querySelectorAll('.innercontent a'), firstTimeShopper = JSON.parse(document.getElementById('configData').innerHTML).isFirstTimeShopper;

if(d_Atag) {
  var i, 
    len = d_Atag.length;

  // IF ANON GHP
  if(hasClass(document.getElementsByTagName("body")[0], "Anonymous")) {
    for(i = 0; i < len; i++) {
      if (d_Atag[i].href.indexOf("Anon_") < 0) {
        d_Atag[i].href = d_Atag[i].href.replace(/GHP_/i, "Anon_");
      }
    }
  }

  // IF FirstTimeShop
  if(firstTimeShopper === "True") {
    for(i = 0; i < len; i++) {
      if (d_Atag[i].href.indexOf("FTS_") < 0) {
        d_Atag[i].href = d_Atag[i].href.replace(/GHP_/i, "1sttime_");
      }
    }
  }

}

// Swaps banner positions , 1C > 1A, 1A > 1B, 1B > 1C
if(firstTimeShopper === "True") {

  //parentNode.replaceChild(newChild, oldChild);
  var node1A = document.getElementById('fs-575'),
    node1B = document.getElementById('fs-576'),
    node1C = document.getElementById('fs-577');

  if(node1A && node1B && node1C){

    var node1AClone = node1A.cloneNode(true),
      node1BClone = node1B.cloneNode(true),
      node1CClone = node1C.cloneNode(true),
      orig1AParent = node1A.parentNode,
      orig1BParent = node1B.parentNode,
      orig1CParent = node1C.parentNode;
    
    //Replace 1C > 1A and change ICID etc..
    orig1AParent.appendChild(node1CClone);
    node1CClone.getElementsByTagName('a')[0].href = node1CClone.getElementsByTagName('a')[0].href.replace(/1C/i, "1A");
    orig1AParent.removeChild(node1A);

    orig1BParent.appendChild(node1AClone);
    node1AClone.getElementsByTagName('a')[0].href = node1AClone.getElementsByTagName('a')[0].href.replace(/1A/i, "1B");
    orig1BParent.removeChild(node1B);

    orig1CParent.appendChild(node1BClone);
    node1BClone.getElementsByTagName('a')[0].href = node1BClone.getElementsByTagName('a')[0].href.replace(/1B/i, "1C");
    orig1CParent.removeChild(node1C);

  }
}

// Since each event button needs its own style for the CTA, it was easier to create this in JS.

// Returns a hex code
function rgb2hex(rgb) {
  if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;
  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  function hex(x) {
    return ("0" + parseInt(x).toString(16)).slice(-2);
  }
  return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

// Returns a RGBA code
function rgb2rgba(rgb, opacity) {
  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  return "rgba(" + rgb[1] + "," + rgb[2] + "," + rgb[3] + "," +  opacity + ")";
}

var eventButtons = document.querySelectorAll('div.innercontent .event-button');

// Check for all event buttons 
if(eventButtons) {
  var i, 
    len = eventButtons.length,
    cssContent = '';
  
  // For each eventButton
  for(i = 0; i < len; i++) {
    // For each eventButton with a style
    if (eventButtons[i].style) {
      var eventButtonColor = eventButtons[i].style.color, 
        eventButtonBackgroundColor = eventButtons[i].style.backgroundColor;
      // For each eventButton with a style and color 
      if(eventButtonColor) {
        // Add line below text on hover based on text color
        cssContent += "#" + eventButtons[i].parentNode.id + ".event-button-wrapper .event-button:hover .link-label {border-bottom-color: " + eventButtonColor + "; }";
        // Adds the colored CTA based on the text color
        cssContent += "#" + eventButtons[i].parentNode.id + ".event-button-wrapper .link-icon {background-image: url(data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2229%22%20height%3D%2229%22%20viewBox%3D%220%200%2058%2058%22%3E%3Cpath%20fill%3D%22"+ rgb2hex(eventButtonColor)+"%22%20d%3D%22M19.917%2015.84a1.352%201.352%200%200%200%200%201.92L31.26%2029.005%2019.917%2040.248a1.356%201.356%200%200%200%200%201.924l1.457%201.44c.534.532%201.41.532%201.943%200l13.785-13.646a1.356%201.356%200%200%200%200-1.924L23.317%2014.396a1.39%201.39%200%200%200-1.943%200l-1.457%201.444z%22%2F%3E%3C%2Fsvg%3E)}";
      }
      if(eventButtonBackgroundColor){
        // Adds the box shadow on hover based on background color
        cssContent += "#" + eventButtons[i].parentNode.id + ".event-button-wrapper .event-button:hover {box-shadow: "+ rgb2rgba(eventButtonBackgroundColor, '0.4') +" 0 0 0 4px;}";
      }
    }
  }

  // Set up <Style>
  var head = document.head || document.getElementsByTagName('head')[0], style = document.createElement('style'); style.type = 'text/css';
  if (style.styleSheet){style.styleSheet.cssText = cssContent;} else {style.appendChild(document.createTextNode(cssContent));}

  // Apend the stylesheet to the header
  head.appendChild(style);

}

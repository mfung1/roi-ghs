// Add Event Function
function addEvent(el, evt, func) {
  if (el.addEventListener){
    return el.addEventListener(evt, func, false);
  } else if(el.attachEvent){
    return el.attachEvent('on' + evt, func);
  }
}

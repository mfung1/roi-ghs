// DLL Carousel Code
var DDLcarousel = document.getElementById("ui-ddl-carousel");

if(DDLcarousel) {
  var activeSlide = 0, 
    numOfSlides = 3,
    carousel_input = document.getElementsByClassName("carousel_input"),
    dots_label = document.getElementsByClassName("dots_label"),
    pauseButton = document.getElementById("ui-ddl-carousel_pause"),
    playButton = document.getElementById("ui-ddl-carousel_play"),
    carouselSlides = document.getElementById("carousel__slides"),
    i = 0,
    scrollBetweenTime = 5000, 
    autoscroll = window.setInterval(function(){ ddlNext(); }, scrollBetweenTime); //Scroll between slides

  addEvent(document.getElementById("ui-ddl-carousel_prev"),'click', ddlPrev);
  addEvent(document.getElementById("ui-ddl-carousel_next"),'click', ddlNext);

  addEvent(pauseButton, 'click', pauseSlide);
  addEvent(playButton, 'click', playSlide);

  // Pause / Play on mouse over / out
  addEvent(carouselSlides, 'mouseover', pauseSlide);
  addEvent(carouselSlides, 'mouseout', playSlide);

  function changeActiveSlide(slide){return function(){
    activeSlide = slide;
  };}

  for (i = 0; i < dots_label.length; i++) {
    addEvent(dots_label[i],'click', changeActiveSlide(i));
  }

  function selectSlide(x){
    for (i = 0; i < carousel_input.length; i++) {
      if(i === x){
        carousel_input[i].checked = true;
        activeSlide = i;
      }
    }
  }

  function ddlPrev(){
    var currentSlide = activeSlide;
    if (currentSlide > 0) {
      selectSlide(currentSlide - 1);
    } else {
      selectSlide(numOfSlides - 1);
    }
  }

  function ddlNext(){
    var currentSlide = activeSlide;
    if (currentSlide < numOfSlides - 1) {
      selectSlide(currentSlide + 1);
    } else {
      selectSlide(0);
    }
  }

  // Pause the auto-rotate
  function pauseSlide() {
    pauseButton.style.display = "none";
    playButton.style.display = "inline-block";
    autoscroll = clearInterval(autoscroll);
  }

  // Play the auto-rotate
  function playSlide(){
    playButton.style.display = "none";
    pauseButton.style.display = "inline-block";
    clearInterval(autoscroll);
    autoscroll = window.setInterval(function(){ ddlNext(); }, scrollBetweenTime);
  }

}

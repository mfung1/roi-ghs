var gulp = require('gulp');

var less = require('gulp-less');
var rename = require("gulp-rename");

var uglify = require("gulp-uglify"),
  concat = require("gulp-concat");

var LessPluginCleanCSS = require('less-plugin-clean-css'),
  cleanCSSPlugin = new LessPluginCleanCSS({advanced: true});

var LessAutoprefix = require('less-plugin-autoprefix'),
  autoprefix = new LessAutoprefix({ browsers: ['ie >= 9', 'last 2 versions'] });

var groupMediaQueries = require('less-plugin-group-css-media-queries');

/* Gulp Task to process less and compress to dist/css/ folder  */
gulp.task('less', function () {
  return gulp.src(['./src/less/**/*.less', '!**/_*/**'])
    .pipe(less({plugins: [autoprefix, groupMediaQueries, cleanCSSPlugin]}))
    .pipe(rename(function (path) {path.basename += ".min";}))
    .pipe(gulp.dest('./dist/css'));
});

/* Gulp Task to process JavaScript and compress to dist/js/ folder  */
gulp.task('minify-js', function () {
    gulp.src(['./src/js/_concat-js/**/*.js', './src/js/**/*.js', '!./src/js/home/**', '!./src/js/global/**']) // path to your files
    .pipe(uglify())
    .pipe(rename(function (path) {path.basename += ".min";}))
    .pipe(gulp.dest('./dist/js'));
});

/* Gulp Task to concatonate HOME JavaScript to dist/js/ folder. Add base folderto ignore in minify-js  */
gulp.task('concat-home', function () {
    gulp.src('./src/js/home/*.js') // path to your files
    .pipe(concat('ghp.js'))  // concat and name it "concat.js"
    .pipe(gulp.dest('./src/js/_concat-js/home'));
});

/* Gulp Task to concatonate GLOBAL JavaScript to dist/js/ folder. Add base folderto ignore in minify-js */
gulp.task('concat-global', function () {
    gulp.src('./src/js/global/*.js') // path to your files
    .pipe(concat('global-marketing.js'))  // concat and name it "concat.js"
    .pipe(gulp.dest('./src/js/_concat-js/global'));
});


// Watch all the above tasks
gulp.task('default', function(){
  gulp.watch('./src/**/*.less', ['less']);
  gulp.watch('./src/**/*.js', ['concat-home', 'concat-global', 'minify-js']);
});